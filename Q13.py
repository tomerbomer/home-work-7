listOfInputs = []
while True:
    num = int(input("Enter number (-1 to quit): "))
    if num == -1:
        break
    listOfInputs.append(num)

setOfInputs = set(listOfInputs)

count = 0
for num in setOfInputs:
    if listOfInputs.count(num) > 1:
        count += 1

print(f"You entered {len(listOfInputs)} numbers, but only "
      f"{len(setOfInputs)} unique numbers.")
print(f"{count} of the numbers were entered multiple times.")


def addSumToFile(file_name):
    """ Reads the numbers from the file and writes the sum to the end of the file.
    :param file_name - the name of file to run the function on. """
    sum = 0
    with open(file_name) as file:
        for line in file:
            sum += int(line)
    with open(file_name, "a") as file:
        file.write(f"\nsum = {sum}")


original_dict = {1: "o", 2: "t", 3: "t", 4: "f", 5: "f", 6: "f", 7: "f", 8: "t", 9: "f", 10: "t"}

unique_letter_set = sorted(set(original_dict.values()))

numberOfLetter_dict = {}

for letter in unique_letter_set:
    numberOfLetter_dict[letter] = [val for val in original_dict.values()].count(letter)

print(numberOfLetter_dict)

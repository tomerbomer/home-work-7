
def checkWordInFile(file_name, word):
    """ Checks if given word is in the given file.
    :param file_name - the name of the file to check in.
    :param word - the word to check for.
    :return True if the word exists in the file. False otherwise. """
    with open(file_name) as searchFile:
        for line in searchFile:
            if word in line:
                return True
    return False

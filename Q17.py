
def reverseFile(input_file, output_file):
    """ Copies the file in reverse to another file.
    :param input_file - the original file to copy from.
    :param output_file - the name of the file to save the reversed copy to. """
    with open(input_file) as file1:
        with open(output_file, "w") as file2:
            for line in file1:
                file2.write(line[::-1])
